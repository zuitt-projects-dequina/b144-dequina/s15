// from morning Session
// Functions
/*
	- Functions are used to store a line of codes that you want to be reused is multiple times on your program
	- it is like a "program" within a program

*/

function printStar() {
	console.log("*");
	console.log("**");
	console.log("***");
}

// printStar();

function sayHello() {
	console.log('Hello');
}
// sayHello();


// the arrangement of the code matters from top to bottom
function alertPrint() {
	alert("Hello"); // alert is use to have a pop-up alert window
	sayHello(); // a function can call a function outside
}

// alertPrint();

// 2 PARAMETERS Function

/* num1, num2 are called PARAMETERS; an additional information need to a funciton to work*/
function addSum(num1,num2) { 
	let sum = num1 + num2;	
	console.log(sum);
}

/* ARGUMENTS - is the actual values of the parameters (e.g. (13,2) ß*/
addSum(13,2); 
addSum(24,56);


function sayHello2(name){
	console.log("Hello " + name);
}

// string must be supplied to the funct if not, it won't work
sayHello2('Zuitt'); 
// because of Coersion, JS would convert 6 to a string type
sayHello2(6);

// 3 PARAMETER Function

function printBio(lname,fname,age) {
	// console.log('Hello ' + lname + " " + fname + age);
	console.log(`Hello Mr. ${lname} ${fname} ${age}`) 
	// this is Interpolation or String Template Literals
	// and this is the newer best practice nowadays
}

// printBio('Start', 'Tony', 50)



// RETURN KEYWORD

function addSum2(x,y) { 
	console.log(x+y)
	return y-x;
	/* used to return a particular value from the function to the function called. You may call the function using the function name and typing it on Chrome Console

		NOTE: the "return func" is the end of the function, thus, any line of code under it won't be read by JS

		example:
			function addSum2(x,y) { 
				return y-x;
				console.log(x+y)
			}
	*/ 
}	

let sumNum = addSum2(3,4);






// Operators

/*
	PMDAS

	Add			+ 			--> Sum
	Subtract 	- 		--> Difference
	Multiply 	*		--> Product
	Divide 		/		--> Quotient
	Modulus 	% 		--> Remainder
	Parenthesis ()		--> Priority

*/

function mod(){
	return 9 % 2;
}

console.log(mod());

// Assignment Operator (=)

	let x = 1;

	let sum = 1;
	sum += 1;

	console.log(sum);

// Increment and Decrement
/*
		This are operators that add or subtrct values by 1 and reassigns the value of the variable where theincrement/decrement by 1
*/

	let z = 1;

// PRE-Increment
	let increment = ++z;
	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

// POST-Increment
	increment = z++;
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);

// PRE-Decrement
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

// PRE-Decrement
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);


// Comparison Operators
	
	// Equality Operator (==) checking if two values are equal or not

		console.log("Equatilty Operator");
		console.log(1 == 1);
		console.log(0 == false); //  type coersion happened
		// Strict Equality (===) - value and type are strictly compared
			console.log(1 === true);

		let juan = 'juan';
		console.log('juan' == juan);

	// Inequality Operator (!=) checking if two values are  not equal

		console.log("Inequality Operator");
		console.log(1 != 1);
		console.log("Juan" != juan); // even the letter casing matters
		// Strict Inequality (!==)
			console.log(0 !== false);

		/* NOTE: Use Strict Equaly/Inequality on control structures to avoid Type Coersion */


	// Other Comparison Opetators
	/*
		>	Greater than
		<	Less thank
		>=	Greater than or equal
		<=	Less than or equal 
	*/


// LOGICAL OPERATORS
	let isLegalAge = true;
	let isRegistered = false;
/*
	AND Operator 	(&&) - returns TRUE if all operands are TRUE
		true && true = true
		true && false = false
		false && true = false
		false & false = false

	OR Operator 	(||) - returns TRUE if at least ONE operands is TRUE
*/
		// AND Operator Sample
		let allRequirementsMet = isLegalAge && isRegistered;
		console.log("Results of logical AND operator: " + allRequirementsMet);

		// OR Operator Sample
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Results of logical OR operator: " + someRequirementsMet);




// Selection Control Structures 

	// IF Statement
	/* 
			- it will execute a statement if a specified condition is TRUE
			
			Syntax: 
				if (condition){
					<statement/s>;
				}
	*/

			let num = -1;

			if (num < 0){
				console.log('Hello');
			}

		// Mini-Activity
		let w = 15;
		if (w >= 10){
			console.log('Welcome to Zuitt!')
		}


	// IF-ELSE Statement
	/* 
			- executes a statement if the conditions are met, else's statement will be executed
			
			Syntax: 
				if (condition){
					<statement/s>;
				}
				else{
					<statement/s>;
				}
	*/
			num = 5;
			if (num >= 10){
				console.log("Number is greater or equal to 10")
			}
			else {
				console.log("Number is not greater or equal to 10")
			}

		// Mini-Activity
		//to make a pop-up window and ask the user an input
		let age = parseInt(prompt("Please provide age:")); 
			if (age > 59){
				alert("Senior age");
			}
			else {
				alert("Invalid age");
			}

	// IF-ELSEIF-ELSE statement 
	/* 
			- accepts multiple condition
			
			Syntax: 
				if (condition){
					<statement/s>;
				}
				elseif{
					<statement/s>;
				}
				else{
					<statement/s>;
				}
	*/
		//example #1
			let city = parseInt(prompt("Enter a number: "));
				if (city === 1){
					alert("Welcome to Quezon City!")
				}
				else if (city === 2){
					alert("Welcome to Valenzuela City!")
				} 
				else if (city === 3){
					alert("Welcome to Pasig City!")
				} 
				else if (city === 4){
					alert("Welcome to Taguig City!")
				}
				else {
					alert("Invalid Number!")
				} 
				 
		//example #2
			let message = '';

			function determineTyphoonIntensity(windSpeed) {
				if (windSpeed < 30) {
					return 'Not a typhoon yet.';
				}
				else if (windSpeed <= 61) {
					return 'Tropical depression detected.';
				}
				else if (windSpeed >= 61 && windSpeed <= 88) {
					return 'Tropical storm detected.';
				}
				else if (windSpeed >= 89 && windSpeed <= 117) {
					return 'Severe tropical storm detected';
				}
				else {
					return 'Typhoon detected.'
				}
			}

			message = determineTyphoonIntensity(70);
			console.log(message);


	// TERNARY Operator
	/*
		The conditional (ternary) operator is the only JavaScript operator that takes three operands: a condition followed by a question mark ( ? ), then an expression to execute if the condition is truthy followed by a colon ( : ), and finally the expression to execute if the condition is falsy.

		Syntax:
			<condition> ? ifTrue : ifFalse
	*/

			let ternaryResult = (1 < 18) ? true : false; // true:false can have other data name
			console.log("Result of Ternary Operator: " + ternaryResult);

			ternaryResult = (1 < 18) ? 'valid' : 'invalid'; 
			console.log("Result of Ternary Operator: " + ternaryResult);


		let name;

		function isOfLegalAge(){
			name = 'John';
			return 'You are of the legal age limit';
		}

		function isUnderAge(){
			name = 'Jane';
			return 'You are under the age limit';
		}

		let age2 = parseInt(prompt("What is your age: "));
		let legalAge = (age2 >= 18) ? isOfLegalAge() : isUnderAge();
		alert("Result of Ternary Operator in functions: " + legalAge + ', ' + name);



//STATEMENTS

	//  SWITCH Statement
	/*
			The switch statement is a part of JavaScript's "Conditional" Statements, which are used to perform different actions based on different conditions. Use switch to select one of many blocks of code to be executed. This is the perfect solution for long, nested if/else statements.

			Syntax: expression = value
				switch (expression){
					case <value 1>:
						<statement/s>;
						break;
					case <value 2>:
						<statement/s>;
						break;
					case <value n>:
						<statement/s>;
						break;
					default:
						<statement/s>;
				}

			NOTE: break; is used to stop the operation once the condition is satisfied.
	*/

		let day = prompt("What day of the week is it today?").toLowerCase();
					// .toLowerCase() is used to lower the case of the input

			switch (day) {
				case 'sunday':
					alert("The color of the day is red");
					break;
				case 'monday':
					alert("The color of the day is orange");
					break;
				case 'tuesday':
					alert("The color of the day is yellow");
					break;
				case 'wednesday':
					alert("The color of the day is green");
					break;
				case 'thursday':
					alert("The color of the day is blue");
					break;
				case 'friday':
					alert("The color of the day is indigo");
					break;
				case 'saturday':
					alert("The color of the day is violet");
					break;
				default:
					alert("Please input valid day");
			}


	// TRY-CATCH-FINALLY Statement - commonly used for error handling, debugging
	/*
			The try statement allows you to define a block of code to be tested for errors while it is being executed. The catch statement allows you to define a block of code to be executed, if an error occurs in the try block. The finally statement lets you execute code, after try and catch, regardless of the result.

			Syntax:

				try {
					<statement>
				}
				catch {
					<statement>
				}
				finally {
					<statement>
				}
	*/
	
	function showIntensityAlert(windSpeed) {
		try {
			alerat(determinneTyphoonIntensity(windSpeed));
		}
		catch (error) {
			console.log(typeof error);
			console.warn(error.message);
		}
		finally {
			alert('Intensity updates will show new alert.')
		}
	}

	showIntensityAlert(70);